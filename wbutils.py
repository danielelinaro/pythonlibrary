
import numpy as np

def alpham(v):
    if v == -35:
        return 1.
    return 0.1 * (v+35.)/(1.-np.exp(-(v+35)/10.))

def betam(v):
    return 4. * np.exp(-(v+60.)/18.)

def minf(v):
    a = alpham(v)
    return a/(a+betam(v))

def taum(v):
    return 1.0/(alpham(v)+betam(v))

def alphah(v):
    return 0.07 * np.exp(-0.05*(v+58.))

def betah(v):
    return 1.0 / (np.exp(-0.1*(v+28.)) + 1.)

def hinf(v):
    a = alphah(v)
    return a/(a+betah(v))

def tauh(v):
    return 1.0/(alphah(v)+betah(v))

def alphan(v):
    if v == -34:
        return 0.1
    return 0.01 * (v+34.)/(1.-np.exp(-(v+34)/10.))

def betan(v):
    return 0.125*np.exp(-0.0125*(v+44.))

def ninf(v):
    a = alphan(v)
    return a/(a+betan(v))

def taun(v):
    return 1.0/(alphan(v)+betan(v))

