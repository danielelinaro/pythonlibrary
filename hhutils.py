
import numpy as np

def vtrap(x,y):
    if np.abs(x/y) < 1e-6:
        return y*(1-x/y/2.);
    return x/(np.exp(x/y)-1.);

def alpham(v):
    return 0.1 * vtrap(-(v+40.),10.)

def betam(v):
    return 4. * np.exp(-(v+65.)/18.)

def minf(v):
    a = alpham(v)
    return a/(a+betam(v))

def taum(v):
    return 1.0/(alpham(v)+betam(v))

def alphah(v):
    return 0.07 * np.exp(-0.05*(v+65.))

def betah(v):
    return 1.0 / (np.exp(-0.1*(v+35.)) + 1.)

def hinf(v):
    a = alphah(v)
    return a/(a+betah(v))

def tauh(v):
    return 1.0/(alphah(v)+betah(v))

def alphan(v):
    return 0.01*vtrap(-(v+55.),10.)

def betan(v):
    return 0.125*np.exp(-0.0125*(v+65.))

def ninf(v):
    a = alphan(v)
    return a/(a+betan(v))

def taun(v):
    return 1.0/(alphan(v)+betan(v))

