
import tables as tbl
import numpy as np

class H5File:
    def __init__(self, filename, mode='w', title='', compressed=True):
        # open the file
        self.fid = tbl.openFile(filename, mode, title)
        self.compressed = compressed
        self.groups = {}
        self.tables = {}
        if self.compressed:
            # create a filter for compression
            self.filter = tbl.Filters(complevel=5, complib='zlib', shuffle=True, fletcher32=False)

    def createGroup(self, where, name, title=''):
        if not name in self.groups:
            self.groups[name] = self.fid.createGroup(where, name, title)
        else:
            print('Group [%s] already in file.' % name)            

    def writeTable(self, groupName, tableName, description, title, elements):
        if groupName in self.groups:
            self.tables[tableName] = self.fid.createTable(self.groups[groupName], tableName, description, title)
            for k,v in elements.iteritems():
                self.tables[tableName].row[k] = v
            self.tables[tableName].row.append()
            self.tables[tableName].flush()
        else:
            print('Group [%s] not in file.' % groupName)
    
    def writeArray(self, groupName, arrayName, atom, data):
        if groupName in self.groups:
            array = self.fid.createCArray(self.groups[groupName], arrayName, atom, data.shape, filters=self.filter)
            if len(data.shape) == 1:
                array[0:] = data
            else:
                for k in range(data.shape[1]):
                    array[0:,k] = data[0:,k]
        else:
            print('Group [%s] not in file.' % groupName)

    def close(self):
        self.fid.close()



